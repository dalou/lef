<?php

namespace Lef;

use Doctrine\ORM\Tools\Setup;
use Doctrine\ORM\EntityManager;
use Doctrine\Common\ClassLoader;
use Doctrine\ORM\Configuration;
use Doctrine\ORM\Mapping\Driver\AnnotationDriver;
use Doctrine\Common\Annotations\AnnotationReader;
use Doctrine\Common\Annotations\AnnotationRegistry;
use Doctrine\Common\Annotations\CachedReader;
use Doctrine\Common\Persistence\Mapping\Driver\MappingDriverChain;
use Doctrine\Common\Cache\ArrayCache;
use Doctrine\Common\Cache\ApcCache;
use Gedmo\DoctrineExtensions;
use Lef\App;

class Config {

	
	private static $envs = array();


	private static $entitiesPath;
	public static function setEntitiesPath($path) {
		self::$entitiesPath = $path;
	}

	private static $connection = array();
	public static function setConnection($connection, $dev=true) {
		self::$connection = $connection;
		self::load("", $dev);
	}


	public static function load($name=null, $dev=false) {

		$paths = array(
		    __DIR__."/Models",
			self::$entitiesPath
		);
		$isDevMode = false;

		date_default_timezone_set('UTC');
		AnnotationRegistry::registerLoader('class_exists');
		//AnnotationRegistry::registerAutoloadNamespace("Lef", __DIR__."/../lef");
		// $reader = new FileCacheReader(
		//     new AnnotationReader(),
		//     "/path/to/cache",
		//     $debug = true
		// );
		$applicationMode = "development";
		// $reader = new AnnotationReader();
		// AnnotationReader::addGlobalIgnoredName('dummy');
		// if (!ENV_PROD) {
		//     $cache = new ArrayCache;
		// } else {
		    $cache = new ArrayCache;
		    //$cache = new \Doctrine\Common\Cache\ArrayCache;
		//}
		// Second configure ORM
		// globally used cache driver, in production use APC or memcached

		// standard annotation reader
		$annotationReader = new AnnotationReader;
		$cachedAnnotationReader = new CachedReader(
		    $annotationReader, // use reader
		    $cache // and a cache driver
		);
		// create a driver chain for metadata reading
		$driverChain = new MappingDriverChain();
		// load superclass metadata mapping only, into driver chain
		// also registers Gedmo annotations.NOTE: you can personalize it
		DoctrineExtensions::registerAbstractMappingIntoDriverChainORM(
		    $driverChain, // our metadata driver chain, to hook into
		    $cachedAnnotationReader // our cached annotation reader
		);

		// now we want to register our application entities,
		// for that we need another metadata driver used for Entity namespace
		$annotationDriver = new AnnotationDriver(
		    $cachedAnnotationReader, // our cached annotation reader
		    $paths // paths to look in
		);
		// NOTE: driver for application Entity can be different, Yaml, Xml or whatever
		// register annotation driver for our application Entity namespace
		$driverChain->addDriver($annotationDriver, 'Lef\Models');
		$driverChain->addDriver($annotationDriver, 'Entities');
		// general ORM configuration
		$config = new Configuration;
		$proxyDir = self::$entitiesPath."/Proxy";
		if(!is_dir($proxyDir)) {
			mkdir($proxyDir, 755);
		}
		$config->setProxyNamespace('Entities\Proxies');
		if ($dev) {
			chmod($proxyDir, 0777);
			$config->setProxyDir($proxyDir); //sys_get_temp_dir());
		    $config->setAutoGenerateProxyClasses(true);
		} else {
			$config->setProxyDir($proxyDir); //sys_get_temp_dir());
		    $config->setAutoGenerateProxyClasses(false);
		}
		// register metadata driver
		$config->setMetadataDriverImpl($driverChain);
		// use our allready initialized cache driver
		$config->setMetadataCacheImpl($cache);
		$config->setQueryCacheImpl($cache);
		// $config = Setup::createConfiguration($isDevMode);
		// $driver = new AnnotationDriver(new AnnotationReader(), $paths);
		// AnnotationRegistry::registerLoader('class_exists');
		// $config->setMetadataDriverImpl($driver);
		// GEDMO
		// Third, create event manager and hook prefered extension listeners
		$evm = new \Doctrine\Common\EventManager();
		// gedmo extension listeners
		// sluggable
		$sluggableListener = new \Gedmo\Sluggable\SluggableListener;
		// you should set the used annotation reader to listener, to avoid creating new one for mapping drivers
		$sluggableListener->setAnnotationReader($cachedAnnotationReader);
		$evm->addEventSubscriber($sluggableListener);
		// tree
		$treeListener = new \Gedmo\Tree\TreeListener;
		$treeListener->setAnnotationReader($cachedAnnotationReader);
		$evm->addEventSubscriber($treeListener);
		// loggable, not used in example
		//$loggableListener = new Gedmo\Loggable\LoggableListener;
		//$loggableListener->setAnnotationReader($cachedAnnotationReader);
		//$loggableListener->setUsername('admin');
		//$evm->addEventSubscriber($loggableListener);
		// timestampable
		$timestampableListener = new \Gedmo\Timestampable\TimestampableListener;
		$timestampableListener->setAnnotationReader($cachedAnnotationReader);
		$evm->addEventSubscriber($timestampableListener);
		// blameable
		$blameableListener = new \Gedmo\Blameable\BlameableListener();
		$blameableListener->setAnnotationReader($cachedAnnotationReader);
		$blameableListener->setUserValue('MyUsername'); // determine from your environment
		$evm->addEventSubscriber($blameableListener);
		// translatable
		$translatableListener = new \Gedmo\Translatable\TranslatableListener;
		// current translation locale should be set from session or hook later into the listener
		// most important, before entity manager is flushed
		$translatableListener->setTranslatableLocale('en');
		$translatableListener->setDefaultLocale('en');
		$translatableListener->setAnnotationReader($cachedAnnotationReader);
		$evm->addEventSubscriber($translatableListener);
		// sortable, not used in example
		//$sortableListener = new Gedmo\Sortable\SortableListener;
		//$sortableListener->setAnnotationReader($cachedAnnotationReader);
		//$evm->addEventSubscriber($sortableListener);
		// mysql set names UTF-8 if required
		$evm->addEventSubscriber(new \Doctrine\DBAL\Event\Listeners\MysqlSessionInit());
		$em = $entityManager = EntityManager::create(self::$connection, $config, $evm);
		App::setEntityManager($entityManager);
	}


	

}