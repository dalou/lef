<?php

namespace Lef;

class Routes {

	
	private static $target;
	public static function getTarget() {
		return self::$target;
	}

	public static $flag;
	public static function hasFlag($name, $params=Array()) {
		if(self::$flag == $name) {
			if(!empty($params) && self::$flag == $name) {
				foreach($params as $name=>$value) {
					if(Request::get($name) != $value) {
						return false;
					}
				}
			}
			return true;
		}
		return false;
	}
	public static function getFlag($pattern=null, $replace=null) {
		if($pattern && $replace) {
			return preg_replace($pattern, $replace, self::$flag);
		}
		return self::$flag;
	}
	public static function setFlag($name) {
		self::$flag = $name;
	}

	private static $routes_named = Array();
	public static function getUrl($name, $params=Array()) {
		$url = '';
		if(!empty(self::$routes_named[$name])) {
			$url = self::$routes_named[$name][0];
		}
		foreach($params as $name=>$param) {
			$url = preg_replace('/:'.$name.'/', $param, $url);
		}
		return $url;
	}

	private static $routes_exp = Array();
	public static function add($urlPattern, $target, $name=null, $flagName=null) {

		$paths = explode('/', trim($urlPattern, '/'));
		$index = &self::$routes_exp;
		$route = Array($urlPattern, $target, $name, $flagName);

		if($name) { 
			self::$routes_named[$name] = $route;
		}

		foreach($paths as $path) {

			if(!empty($path)) {
				if(substr($path, 0, 1) == ':') {
					if(empty($index[':'])) {
						$index[':'] = Array(substr($path, 1));
						$index[':'][1] = Array();						
					}
					$index = &$index[':'][1];
				}
				else {
					if(empty($index[$path])) { 
						$index[$path] = Array();
					}
					$index = &$index[$path];
				}	
			}			
		}
		$index[''] = $route;
	}
	

	public static function handle() {

	    // $_SERVER['REQUEST_URI'];
	    // $_SERVER['REQUEST_METHOD'];
	    if(empty($_SERVER['REQUEST_URI'])) { return; }

	    $paths = explode('/', trim(explode('?', $_SERVER['REQUEST_URI'])[0], '/'));
	    $route = self::handleTree($paths, self::$routes_exp);

	    if(!empty($route)) {
	    	if(!empty($route[3])) {
	    		self::$flag = $route[3];
	    	}
	    	else if(!empty($route[2])) {
	    		self::$flag = $route[2];
	    	}
	    	self::$target = $route[1];
	    }
	    else {
	    	self::$target = '/main/404.php';
	    }
	}


	public static function handleTree($paths, $routes, $index=0) {



		// echo '<br/> Itération: '.$index. ' <br/>';
		// print_r($routes);
		if(!empty($paths[$index])) {
			$path = $paths[$index];
			//echo $path. ' ';
			if(!empty($routes[$path])) {
				return self::handleTree($paths, $routes[$path], $index+1);
			}
			else if(!empty($routes[':'])) {	
				$argName = $routes[':'][0];
				Request::set($argName, $path);
				//echo '+ARG '.$argName;
				return self::handleTree($paths, $routes[':'][1], $index+1);
			}
			else {

			}
		}
		else if(!empty($routes[''])) {
			return $routes[''];
		}
		return null;
	}

}