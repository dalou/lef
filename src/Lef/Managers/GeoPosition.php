<?php

namespace Lef\Managers;

use Lef\Managers\Dated;
use Lef\App;

class GeoPosition extends Dated
{ 
    
    function getBoundingRectangle($lat_degrees,$lon_degrees, $distance_in_km) {

        $distance_in_miles =  $distance_in_km / 1.609344; // 1 mile = 1.609344 kilometers
        $radius = 3963.1; // of earth in miles       

        // bearings - FIX   
        $due_north = deg2rad(0);
        $due_south = deg2rad(180);
        $due_east = deg2rad(90);
        $due_west = deg2rad(270);

        // convert latitude and longitude into radians 
        $lat_r = deg2rad($lat_degrees);
        $lon_r = deg2rad($lon_degrees);

        $northmost  = asin(sin($lat_r) * cos($distance_in_miles/$radius) + cos($lat_r) * sin ($distance_in_miles/$radius) * cos($due_north));
        $southmost  = asin(sin($lat_r) * cos($distance_in_miles/$radius) + cos($lat_r) * sin ($distance_in_miles/$radius) * cos($due_south));

        $eastmost = $lon_r + atan2(sin($due_east)*sin($distance_in_miles/$radius)*cos($lat_r),cos($distance_in_miles/$radius)-sin($lat_r)*sin($lat_r));
        $westmost = $lon_r + atan2(sin($due_west)*sin($distance_in_miles/$radius)*cos($lat_r),cos($distance_in_miles/$radius)-sin($lat_r)*sin($lat_r));

        $northmost = rad2deg($northmost);
        $southmost = rad2deg($southmost);
        $eastmost = rad2deg($eastmost);
        $westmost = rad2deg($westmost);

        // sort the lat and long so that we can use them for a between query        
        if ($northmost > $southmost) { 
            $lat1 = $southmost;
            $lat2 = $northmost;
        } else {
            $lat1 = $northmost;
            $lat2 = $southmost;
        }
        if ($eastmost > $westmost) { 
            $lon1 = $westmost;
            $lon2 = $eastmost;
        } else {
            $lon1 = $eastmost;
            $lon2 = $westmost;
        }
        return array($lat1,$lat2,$lon1,$lon2);
    }

    public function findArround($lat, $lng, $dist=150, $limit=20) {

        list($minLat, $maxLat, $minLon, $maxLon) = $this->getBoundingRectangle($lat, $lng, $dist);

        $query = $this->createQueryBuilder('gp')

        //$query = $this->_em->createQuery('SELECT * FROM Entities\Service gp')
            ->where('gp.latitude >= :lat1')
            ->setParameter('lat1', $minLat)
            ->andWhere('gp.latitude <= :lat2')
            ->setParameter('lat2', $maxLat)
            ->andWhere('gp.longitude >= :lng1')
            ->setParameter('lng1', $minLon)
            ->andWhere('gp.longitude <= :lng2')
            ->setParameter('lng2', $maxLon)
            //->having('acos(sin(1.3963) * sin(Lat) + cos(1.3963) * cos(Lat) * cos(Lon - (-0.6981))) <= 0.1570')
        ;
        // print_r(array(
        //     'sql'        => $query->getQuery()->getSQL(),
        //     'parameters' => $query->getQuery()->getParameters(),
        // ));

        $query->setFirstResult(0)
            ->setMaxResults($limit);
        $results = $query->getQuery()->getResult();

        $results_limit = array();
        foreach($results as $result) {
            $dist = $result->getDistanceFrom($lat, $lng, 'K', false);
            // while(!empty($results_limit[$dist])) {
            //     $dist .= '_0';
            // }
            $results_limit[$dist] = $result;
        }
        ksort($results_limit);
        // $results = array(); 
        // $i=0;
        // foreach($results_limit as $result) {
        //     if($i>$limit) { break; }
        //     $results[] = $result;
        //     $i++;
        // }
        
        $results = array_slice($results_limit, 0, $limit);

       
        // SELECT * FROM Places WHERE
        //     (Lat >= 1.2393 AND Lat <= 1.5532) AND (Lon >= -1.8184 AND Lon <= 0.4221)
        // HAVING
        //     acos(sin(1.3963) * sin(Lat) + cos(1.3963) * cos(Lat) * cos(Lon - (-0.6981))) <= 0.1570;

        return $results;
 
    }

    public function findByBoundingRectange($lat1, $lng1, $lat2, $lng2, $limit=50) {


    	$query = $this->createQueryBuilder('gp')
            ->where('gp.latitude => :lat1')
            ->setParameter('lat1', $lat1)
            ->andWhere('gp.latitude <= :lat2')
            ->setParameter('lat2', $lat2)
            ->andWhere('gp.longitude => :lng1')
            ->setParameter('lng1', $lng1)
            ->andWhere('vr.longitude <= :lng2')
            ->setParameter('lng2', $lng2)
            ->setFirstResult(0)
            ->setMaxResults($limit);  
        return $query->getQuery()->getResult();
    }
}