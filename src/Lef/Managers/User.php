<?php

namespace Lef\Managers;

use Doctrine\ORM\EntityRepository;
use Lef\Libs\Facebook\Facebook;
use Lef\Libs\Facebook\FacebookApiException;
use Lef\App;
use Lef\Request;

class User extends Dated
{

    protected $facebookAppId;
    protected $facebookSecret;
    protected $user;


    public function loginWithFacebook() {

        $facebook = new Facebook(array(
          'appId'  => $this->facebookAppId,
          'secret' => $this->facebookSecret
        ));

        // Get User ID
        $facebookId = $facebook->getUser();
        if ($facebookId) {
          try {
            $user_profile = $facebook->api('/me');
          } catch (FacebookApiException $e) {
            error_log($e);
            $facebookId = null;
          }
        }
        if ($facebookId) {

            $userProfile = $facebook->api('/me');
            $user = $this->findOneByFacebookId($facebookId);
            if($user) {

            }
            else {
                $username = $userProfile['username'];
                $i = 1;
                while($this->findOneByUsername($username) != null) {
                    $username = $userProfile['username'].'_'.$i;
                    $i++;
                }
                $user = $this->createUser($username, 'test');
                $user->setFacebookId($facebookId);
                $user->setFirstname($userProfile['first_name']);
                $user->setLastname($userProfile['last_name']);                
                App::$em->persist($user);
                App::$em->flush();
            }
            $this->logIn($user, array('logoutUrl' => $facebook->getLogoutUrl()));
            Request::redirect('/');

            // $user = user::getByFacebookId($fb_id, $facebook);
        } else {
          Request::redirect($facebook->getLoginUrl());
        }
    }

    public function createUser($email, $password, $level='MEMBER') {

        $user = $this->findOneByEmail($email);
        if($user) {
            return null;
        }
        $user = new \Entities\User();
        $user->setEmail($email);
        $user->setPlainPassword($password);
        $user->setLevel($level);
        return $user;
    }

    public function getUser() {

        if($this->user) { return $this->user; }
 
        if(!empty($_SESSION['user']['logged']) && $user = $this->findOneById($_SESSION['user']['logged'])) {
            
        }
        else if(!empty($_COOKIE['REMEMBER_ME']) && $user = $this->findOneByLoginSession($_COOKIE['REMEMBER_ME'])) {
           
        }
        else {
            $user = new \Entities\User();
            $user->setLevel("VISITOR");
            $user->setUsername("Visiteur");            
        }      
        $this->user = $user;  
        return $user;
    }

    public function logIn($user, $rememberMe=false, $extra=Array()) {
        if(!$user->getId()) {
            App::$em->persist($user);
            App::$em->flush();
        }
        if(empty($_SESSION['user'])) {
            $_SESSION['user'] = Array();
        }
        $_SESSION['user']['logged'] = $user->getId();
        foreach($extra as $key=>$value) {
            $_SESSION['user'][$key] = $value;
        }
        if($rememberMe) {

            $cookiehash = md5(sha1($user->getEmail() . $user->getId() . time() . rand(1,1000)));
            $user->setLoginSession($cookiehash);
            $params = session_get_cookie_params();
            setcookie('REMEMBER_ME', $cookiehash, time() + 60*60*24*30, $params["path"], $params["domain"], $params["secure"], $params["httponly"]);
        }
        else {
            $user->setLoginSession(NULL);
        }
        App::$em->persist($user);
        App::$em->flush();
        


        //setcookie("user", $_SESSION['user'], time()+60*60*24*100,'/');
    }

    public function logOut() {

        $params = session_get_cookie_params();
        setcookie('REMEMBER_ME', "", time() - 3600, $params["path"], $params["domain"], $params["secure"], $params["httponly"]);  
        unset($_COOKIE['REMEMBER_ME']);


        if(isset($_SESSION['user']['logged'])) {
            $_SESSION['user']['logged'] = null;
            unset($_SESSION['user']['logged']);
            unset($_SESSION['user']);
        }
        if(isset($_SESSION['user'])) {
            unset($_SESSION['user']);
        }

        
        //Request::redirect($redirect);
    }
}