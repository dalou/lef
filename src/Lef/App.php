<?php

namespace Lef;

class App {

	public static $entityManager;
	public static $em; // shortcut
	public static $user; // shortcut
	private static $data = Array();
	private static $metas = Array();
	public static $templatesPath = '../src/Templates';

	public static function setEntityManager($entityManager) {
		self::$entityManager = self::$em = $entityManager;
	}

	public static function getEntityManager() {
		return self::$entityManager;
	}

	public static function setTemplatesPath($path) {
		self::$templatesPath = $path;
	}

	public static function run() {
		self::loadTarget(realpath(self::$templatesPath . '/' . Routes::getTarget()));
	}

	public static function loadTarget($target) {
		if(is_file($target)) {
			require($target);
		}
		else {

		}
		if( !empty($_SESSION['flashMessageDelete']) && !empty($_SESSION['flashMessage'])) {
			unset($_SESSION['flashMessage']);
			unset($_SESSION['flashMessageDelete']);
		}
	}

	public static function setMeta($name, $value) {
		self::$metas[$name] = $value;
	}

	public static function getMeta($name, $default='') {
		return !empty(self::$metas[$name]) ? self::$metas[$name] : $default;
	}

	public static function setFlashMessage($message) {
		$_SESSION['flashMessage'] = $message;
	}

	public static function getFlashMessage() {
		if( !empty($_SESSION['flashMessage'])) {
			$_SESSION['flashMessageDelete'] = true;
			$message = $_SESSION['flashMessage'];
			return $message;
		}
		else {
			return null;
		}
	}

	public static function getManager($name) {
		return self::$entityManager->getRepository($name);
	}

	public static function createQuery($args) {
		return self::$entityManager->createQuery($args);
	}

	public static function get($name) {
		return !empty(self::$data[$name]) ? self::$data[$name] : null;
	}

	public static function getUser($name) {
		
		return self::$em->getManager('Entities\User')->getUser();
	}

	public static function set($name, $value) {
		self::$data[$name] = $value;
	}
	
	

}