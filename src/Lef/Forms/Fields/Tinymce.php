<?php

namespace Lef\Forms\Fields;

class Tinymce extends Textarea
{	
	public function build($form, $options) {
		$this->addAttr('class', 'form-control tinymce');
		$this->addAttr('style', 'height:300px;');

		$this->optionsSelector = !empty($options['selector']) ? $options['selector'] : "#:id";
		$this->optionsExternalPlugins = !empty($options['external_plugins']) ? $options['external_plugins'] : array();
		$this->optionsPlugins = !empty($options['plugins']) ? $options['plugins'] : array(
			"advlist autolink lists link image charmap print preview anchor",
			"searchreplace visualblocks code fullscreen",
			"insertdatetime media table contextmenu paste"
		);
		$this->optionsToolbar = !empty($options['toolbar']) ? $options['toolbar'] : "insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image | code";
		$this->optionsMenubar = !empty($options['menubar']) ? $options['menubar'] : false;

		$this->setInputTemplate('<textarea :attrs>:value</textarea>
			<script src="//tinymce.cachefly.net/4.0/tinymce.min.js"></script>
			<script>
			        tinymce.init({
			        	selector: '.json_encode($this->optionsSelector).',
			        	external_plugins: '.json_encode($this->optionsExternalPlugins).',
			        	plugins: '.json_encode($this->optionsPlugins).',
					    toolbar: '.json_encode($this->optionsToolbar).',
					    menubar: '.json_encode($this->optionsMenubar).'
			        });
			</script>');
	}
		// # Custom buttons
 //    tinymce_buttons:
 //        stfalcon: # Id of the first button
 //            title: "Stfalcon"
 //            image: "http://stfalcon.com/favicon.ico"

 //    external_plugins:
 //        imageupload:
 //            url: "asset[bundles/piyweb/tinymce/imageupload/editor_plugin.js]"
 //    theme:
 //        # Simple theme: same as default theme
 //        simple: ~
 //        # Advanced theme with almost all enabled plugins
 //        advanced:
 //             plugins:
 //                 - "advlist autolink lists link image charmap print preview hr anchor pagebreak"
 //                 - "searchreplace wordcount visualblocks visualchars code fullscreen"
 //                 - "insertdatetime media nonbreaking save table contextmenu directionality"
 //                 - "emoticons template paste textcolor"
 //             toolbar1: "insertfile undo redo | styleselect | bold italic | images | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image"
 //             toolbar2: "print preview media | forecolor backcolor emoticons | stfalcon | example"
 //             image_advtab: true
 //             templates:
 //                 - {title: 'Test template 1', content: 'Test 1'}
 //                 - {title: 'Test template 2', content: 'Test 2'}
 //        # BBCode tag compatible theme (see http://www.bbcode.org/reference.php)
 //        bbcode:
 //             plugins: 
 //                - "code, link, preview, imageupload, media, image"
 //                - "insertdatetime media nonbreaking save table contextmenu directionality"
 //                - "emoticons template paste textcolor"
 //             menubar: false
 //             toolbar1: "bold,italic,underline | alignleft aligncenter alignright alignjustify| image media imageupload | forecolor backcolor emoticons | undo,redo | link,unlink,removeformat,cleanup | code,preview"
 //             height: 300

}