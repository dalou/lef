<?php

namespace Lef\Forms\Fields;
use Lef\Forms\Fields\Field;

class ImageEntityCollection extends Field
{

    private $maxImages = 6;

	public function build($form, $options) { 
        $this->className = !empty($options['class']) ? $options['class'] : '';
        $this->addLink = !empty($options['addLink']) ? $options['addLink'] : '+ Image';
        $this->updateLink = !empty($options['updateLink']) ? $options['updateLink'] : 'Changer';
   	}

	public function bind($values) {
		$entity = $this->getForm()->getEntity();
		$images = $entity->getImages();
		for( $i=0; $i<$this->maxImages; $i++) {
			$image = !empty($images[$i]) ? $images[$i] : null;
			if(!empty($values[$i])) {

				if($values[$i] == 'delete') {
					if($image) {
						$entity->removeImage($image);
						//unset($images[$i]);
					}
				}
				else {
					if(!$image) {
                        $image = new $this->className();
                    }
					if( $image->setUploadedFile($values[$i]) ) {
						$entity->addImage($image);
						//unset($images[$i]);
					}
					else {
						$this->setError($image->getFileError());
					}
				}				
			}			
		}
		$this->setValue($images);
	}

	public function renderInput($template=null) {

		$images = $this->getValue();
        $html = '
        <div class="row">';
		for( $i=0; $i<$this->maxImages; $i++) {
			$html .= '
            <div class="col-md-2 '.$this->getId().'" style="position:relative;">
				<img style="width:150px; '.( empty($images[$i]) ? "display:none;" : "").'" class="img-thumbnail img-responsive" src="'.( empty($images[$i]) ? "" : $images[$i]->getWebPath()).'" />
				<a class="remove-image btn btn-danger btn-small" style="
                    position:absolute;
                    right:1px;
                    top: 1px;
                     '.( empty($images[$i]) ? "display:none;" : "").'">
                    <span class="glyphicon glyphicon-remove"></span>
                </a>
				<input  type="file" 
                        class="btn btn-? btn-small" 
						data-index="'.$i.'"
						id="'.$this->getId().'_'.$i.'" 
						value="" name="'.$this->getName().'['.$i.']" 
						title="'.( empty($images[$i]) ? $this->addLink : $this->updateLink).'">
                <input  type="hidden" value="delete" name="" >
			</div>';
		}
		$html .= '
        </div>

		<script type="text/javascript">
		$(document).ready(function(){		
            $("input[type=file]").bootstrapFileInput();
			$(".'.$this->getId().'").each(function(i, self, file, img, hidden, name, remove) {

                file = $(self).find("input[type=file]");
                name = file.attr("name");
                hidden = $(self).find("input[type=hidden]");
                img = $(self).find("img");
                remove = $(self).find("a.remove-image");

                file.change(function(){
                    if (this.files && this.files[0]) {
                        var reader = new FileReader();                      
                        reader.onload = function (e) {
                            remove.show();
                            hidden.attr("name", "");
                            file.attr("name", name).prev().text("'.$this->updateLink.'");
                            img.show().attr("src", e.target.result);
                        }                       
                        reader.readAsDataURL(this.files[0]);
                    }
                })//.bootstrapFileInput();
                remove.click(function() {
                    file.attr("name", "").prev().html("'.$this->addLink.'");
                    hidden.attr("name", name);
                    $(self).find(".file-input-name").remove();
                    img.hide();
                    remove.hide();
                });

            })
		});
		</script>';	

		return $html;
	}

}