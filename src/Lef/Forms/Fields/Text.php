<?php

namespace Lef\Forms\Fields;

class Text extends Field
{
	public function build($form, $options) {
		$this->addAttr('value', ':value');
	}
}