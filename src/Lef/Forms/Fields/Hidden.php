<?php

namespace Lef\Forms\Fields;

class Hidden extends Field
{

	public function build($form, $options) {
		$this->setTemplate(':input');
		$this->addAttr('value', ':value');
		$this->addAttr('type', 'hidden');
	}

	public function valid() {
	}

}