<?php

namespace Lef\Forms\Fields;

class GenericImage extends File
{

	public function build($form, $options) {
	}

	public function bind($value) {
		$rp = \App::getManager('Entities\GenericImage');
		$entity = $rp->find($value);
		$this->setValue($entity);
	}

	public function renderInput() {

		$entity = $this->getValue() ? $this->getValue() : new \Entities\GenericImage();

		return '<div class="ro">
                  <div class="col-lg2">
                    <input class="form-control" type="file" name="'.$this->getName().'" id="'.$this->getId().'" value="'.$entity->getId().'">                   
                    
                    <script type="text/javascript">
                   		console.log($.fn.typeahead)
                        $("#'.$this->getId().'").prev().typeahead([
                          {
                            name: "nba-teams",
                            remote: "/geoCounty/autoComplete/?query=%QUERY",
                            template: "<p>{{fullname}}</p>",
                            engine: Hogan
                          }
                        ]).bind("typeahead:selected", function(e, data) {
                            $("#'.$this->getId().'").val(data.id)
                                .next().text(data.postalCode)
                                .next().text(data.adminName1)
                                .next().text(data.adminName2)
                        });
                    </script>
                  </div>
                </div>';
	}

	// public function render() {
	// 	return '<div class="form-group">
	// 		<label for="" class="col-lg-3 control-label">'.$this->label.'</label>
	// 		<div class="col-lg-9">
	// 			'.$this->renderInput().'
	// 		</div>
	// 		'.$this->renderError().'
	// 	</div>';
	// }
}