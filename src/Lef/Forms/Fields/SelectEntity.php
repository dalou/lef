<?php

namespace Lef\Forms\Fields;

use Lef\App;

class SelectEntity extends Field
{

	private $choices = Array();

	public function build($form, $options) {
		$this->choices = !empty($options['choices']) ? $options['choices'] : Array();
		$this->className = !empty($options['class']) ? $options['class'] : '';
		if(empty($this->choices)) {
			$entity = $this->getForm()->getEntity();
			if($entity) {	
				$setter = 'get'.ucfirst($this->getFieldName()).'Choices';
				if(method_exists($entity, $setter)) {
					$this->choices = $entity->$setter();
				}
			}
		}
	}

	public function bind($value) {
		$entity = $this->getForm()->getEntity();
		if($entity) {
			$value = App::getManager($this->className)->find($value);	
			$this->setValue($value);	
		}	
	}

	public function renderInput($template=null) {

		$output = '<select class="form-control" name="'.$this->getName().'" id="'.$this->getId().'">';
		foreach($this->choices as $value=>$title) {
			$output .= '<option '.($this->getValue() && $value == $this->getValue()->getId() ? 'selected="selected"': '').' value="'.$value.'">'.$title.'</option>';
		}
		$output .= '</select>';
		return $output;
	}

}