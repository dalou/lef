<?php

namespace Lef\Forms\Fields;

class Password extends Field
{
	public function build($form, $options) {
		$this->addAttr('type', 'password');
	}

}