<?php

namespace Lef\Forms\Fields;

class Field
{

	private $value;
	private $name;
	private $required;
	private $error;
	private $form;
	public $isRendered = false;

	private $attrs = array();
	public function addAttr($name, $value) {
		$this->attrs[$name] = $value;
	}

	protected $inputTemplate;
	public function setInputTemplate($inputTemplate) {
		$this->inputTemplate = $inputTemplate;
	}

	protected $errorTemplate;
	public function setErrorTemplate($errorTemplate) {
		$this->errorTemplate = $errorTemplate;
	}

	protected $template;
	public function setTemplate($template) {
		$this->template = $template;
	}
	public function __construct($form, $name, $options=Array()) {
		$this->form = $form;
		$this->name = $name;
		$this->value = $this->getEntityValue();
		$this->error = null;

		$this->addAttr('id', $this->getId());
		$this->addAttr('name', $this->getName());
		$this->addAttr('type', 'text');
		$this->addAttr('class', 'form-control');
		$this->attrs = !empty($options['attrs']) ? array_merge($this->attrs, $options['attrs']) : $this->attrs;
		
		$this->setTemplate(!empty($options['template']) ? $options['template'] : 
			'<div class="form-group :error?has-error ">
			  <label class="col-sm-2 control-label" for=":id">
			  	:label :error
			  </label>
			  <div class="col-sm-10">:input</div>
			</div>');

		$this->setInputTemplate(!empty($options['inputTemplate']) ? $options['inputTemplate'] : 
			'<input :attrs />');

		$this->setErrorTemplate(!empty($options['errorTemplate']) ? $options['errorTemplate'] : 
			'<small class="error">:error</small>');

		$this->required = !empty($options['required']) ? $options['required'] : null;
		if(!empty($this->required)) {
			$this->addAttr('required', 'required');
		}

		$this->label = !empty($options['label']) ? $options['label'] : '';

		$this->build($form, $options);
	}	

	public function build($form, $options) {
		$this->addAttr('value', ':value');
	}

	public function setValue($value) { $this->value = $value; }
	public function getValue() { return $this->value; }
	public function setEntityValue($value) {
		$entity = $this->getForm()->getEntity();
		if($entity) {		
			$setter = 'set'.ucfirst($this->getFieldName());
			if(method_exists($entity, $setter)) {
				$value = $entity->$setter($value);
			}
		}
	}
	public function getEntityValue() { 
		$entity = $this->getForm()->getEntity();
		if($entity) {				
			$getter = 'get'.ucfirst($this->getFieldName());
			if(method_exists($entity, $getter)) {
				return $entity->$getter(); 
			}
			else {
				$getter = 'is'.ucfirst($this->getFieldName());
				if(method_exists($entity, $getter)) {
					return $entity->$getter() ? true : false; 
				}
			}
		}
		return null;
	}
	public function getForm() { return $this->form; }
	public function getFieldName() { return $this->name; }
	public function getName() {
		return $this->getForm()->getName().'['.$this->name.']'.($this->getForm()->getParentField() ? '' : '');
	}
	public function getId() {
		return preg_replace('/\[|\]/i', '_', $this->getName());
	}
	public function getError() { return $this->error; }
	public function setError($error) { 
		if(empty($error)) {
			return;
		}
		$this->error = $error; 
		$this->getForm()->addError($this->getName(), $error);
	}
	public function isRequired() { return !empty($this->required); }
	public function getRequired() { return $this->required; }
	public function getLabel() { return $this->label; }
	public function bind($value) {
		$this->setValue($value);
	}
	public function valid() { } 
	public function save() {
		$this->setEntityValue($this->getValue());
	}
	public function renderInput($template=null) {

		$output =  $template ? $template : $this->inputTemplate;
		$attrs = '';
		foreach($this->attrs as $name=>$value) {
			$attrs .= $name.'="'.$value.'" ';
		}
		$output = preg_replace('/:attrs/', $attrs, $output);
		$output = preg_replace('/:value/', $this->getValue(), $output);
		$output = preg_replace('/:name/', $this->getName(), $output);
		$output = preg_replace('/:id/', $this->getId(), $output);
		$output = preg_replace('/:label/', $this->getLabel(), $output);
		$output = preg_replace('/:error/', $this->renderError(), $output);
		$this->isRendered = true;
		return $output;

		// $output = $this->template;
		// $output = preg_replace('/:label/', $this->getLabel(), $output);
		// return '<input type="text" id="'.$this->getId().'" value="'.$this->getValue().'" name="'.$this->getName().'" required="required" maxlength="255" class="form-control">';
	}
	public function renderError($template=null) {
		$output =  $template ? $template : $this->errorTemplate;
		$output = preg_replace('/:error/', $this->getError(), $output);
		return $output;
	}
	public function render($template=null) {

		$output = $template ? $template : $this->template;
		$output = preg_replace('/:label/', $this->getLabel(), $output);
		$output = preg_replace('/:error\?([a-zA-Z-_"=]+)/', $this->getError() ? '\\1':'', $output);
		$output = preg_replace('/:error/', $this->renderError(), $output);
		$output = preg_replace('/:input/', $this->renderInput(), $output);
		$output = preg_replace('/:id/', $this->getId(), $output);
		$this->isRendered = true;
		return $output;

		// return '<div class="form-group '.( $this->getError() ? 'has-error':'' ).'">
		//   <label class="control-label" for="'.$this->getId().'">'
		//   	.$this->getLabel().''.( $this->getError() ? ' : '.$this->renderError():'' ).'
		//   </label>
		//   '.$this->renderInput().'
		// </div>';
	}
}