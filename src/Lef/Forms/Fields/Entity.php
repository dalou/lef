<?php

namespace Lef\Forms\Fields;

class Entity extends Field
{

  private $class; 

	public function build($form, $options) {
    	$this->className = !empty($options['class']) ? $options['class'] : null;
	}

	public function bind($value) {
		$entity = $this->getForm()->getEntity();
		if($entity) {
			$value = App::getManager($this->className)->find($value);	
			$this->setValue($value);	
		}	
	}

  public function getValue() { return $this->value ? $this->value : new $this->className(); }


	// public function render() {
	// 	return '<div class="form-group">
	// 		<label for="" class="col-lg-3 control-label">'.$this->label.'</label>
	// 		<div class="col-lg-9">
	// 			'.$this->renderInput().'
	// 		</div>
	// 		'.$this->renderError().'
	// 	</div>';
	// }
}