<?php

namespace Lef\Forms\Fields;

class Select extends Field
{

	private $choices = Array();

	public function build($form, $options) {
		$this->choices = !empty($options['choices']) ? $options['choices'] : Array();
		if(empty($this->choices)) {
			$entity = $this->getForm()->getEntity();
			if($entity) {	
				$setter = 'get'.ucfirst($this->getFieldName()).'Choices';
				if(method_exists($entity, $setter)) {
					$this->choices = $entity->$setter();
				}
			}
		}
	}

	public function renderInput($template=null) {
		$output = '<select class="form-control" name="'.$this->getName().'" id="'.$this->getId().'">';
		foreach($this->choices as $value=>$title) {
			$output .= '<option '.($value == $this->getValue() ? 'selected="selected"': '').' value="'.$value.'">'.$title.'</option>';
		}
		$output .= '</select>';
		return $output;
	}

}