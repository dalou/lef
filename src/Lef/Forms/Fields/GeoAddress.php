<?php

namespace Lef\Forms\Fields;

class GeoAddress extends Field
{

	private $geoPositionsFields;

	public function build($form, $options) {

		$this->addAttr('value', ':value');
		$this->addAttr('style', 'padding-right:50px;');

		$this->geoPositionsFields = array(
			'latitude', 'longitude', 'placeName', 'placeType', 
			'streetNumber', 'route', 'locality', 
			'administrativeAreaLevel2', 'administrativeAreaLevel1',
			'country','postalCode'
		);

		$form = $this->getForm();
		$geoPosition = $form->getEntity();
		$lat = -34.397;
		$lng = 150.644;

		if($geoPosition) {
			foreach($this->geoPositionsFields as $fieldName) {
				$form->addField($fieldName, 'Lef\Forms\Fields\Hidden');
			}
			$tmpLat = $geoPosition->getLatitude();
			$tmpLng = $geoPosition->getLongitude();
			if(!empty($tmpLat) && !empty($tmpLng)) {
				$lat = $tmpLat;
				$lng = $tmpLng;
			}
		}

		$inputTemplate = '';
		$inputTemplate .= '

		<input :attrs />
		<button id="geo_locate_:id" class="btn btn-sm" style="position: absolute; top:2px; right:17px;"><span class="glyphicon glyphicon-screenshot"></span></button>


		<script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?libraries=geometry&v=3.exp&sensor=true"></script>
		<div id="map-canvas-:id" style="height: 150px;" class="form-control"></div>
		';

		if($geoPosition) {
			$inputTemplate .= '<small id="geocoding-info-:id">Geocoding : '.$geoPosition->getFormattedAddress().' (lat: '.$geoPosition->getLatitude().', lng: '.$geoPosition->getLongitude().')</small>';
			foreach($this->geoPositionsFields as $fieldName) {
				$inputTemplate .= $form->getField($fieldName)->renderInput();				
			}
		}

		$inputTemplate .= '
		<script type="text/javascript">			

			$(document).ready(function(map, mapId, marker, locator, geocoder, locate) {

				mapId = "map-canvas-:id";
				locator = $("#geo_locate_:id");

		 	    geocoder = new google.maps.Geocoder();
				var latlng = new google.maps.LatLng('.$lat.', '.$lng.');
				var mapOptions = {
					zoom: 18,
					center: latlng
				}
				map = new google.maps.Map(document.getElementById(mapId), mapOptions);
				marker = new google.maps.Marker({
				    map: map,
				    position: latlng
				});

				locator.click(function() {
					var address = $("#:id").val();
					if($.trim(address) != "") { locate(address); }				
					return false;
				})

				locate = function(address) {
					geocoder.geocode( { "address": address }, function(results, status) {
						if (status == google.maps.GeocoderStatus.OK) {
							map.setCenter(results[0].geometry.location);
							marker = new google.maps.Marker({
							    map: map,
							    position: results[0].geometry.location
							});

							$("#'.$form->getField('latitude')->getId().'").val(results[0].geometry.location.lat());
					      	$("#'.$form->getField('longitude')->getId().'").val(results[0].geometry.location.lng());
					      	$("#'.$form->getField('placeName')->getId().'").val(results[0].formatted_address);
					      	$("#'.$form->getField('placeType')->getId().'").val(results[0].types[0]);

					      	$("#geocoding-info-:id").text("Geocoding : "+results[0].formatted_address+" (lat: "+results[0].geometry.location.lat()+", lng: "+results[0].geometry.location.lng()+")");

					      	for(var i in results[0].address_components) {
					      		var comp = results[0].address_components[i];
					      		if(comp.types[0] == "street_number") {
					      			$("#'.$form->getField('streetNumber')->getId().'").val(comp.long_name);
					      		}
					      		else if(comp.types[0] == "route") {
					      			$("#'.$form->getField('route')->getId().'").val(comp.long_name);
					      		}
					      		else if(comp.types[0] == "locality") {
					      			$("#'.$form->getField('locality')->getId().'").val(comp.long_name);
					      		}
					      		else if(comp.types[0] == "administrative_area_level_2") {
					      			$("#'.$form->getField('administrativeAreaLevel2')->getId().'").val(comp.long_name);
					      		}
					      		else if(comp.types[0] == "administrative_area_level_1") {
					      			$("#'.$form->getField('administrativeAreaLevel1')->getId().'").val(comp.long_name);
					      		}
					      		else if(comp.types[0] == "country") {
					      			$("#'.$form->getField('country')->getId().'").val(comp.long_name);
					      		}
					      		else if(comp.types[0] == "postal_code") {
					      			$("#'.$form->getField('postalCode')->getId().'").val(comp.long_name);
					      		}
					      	}

						} else if(status == google.maps.GeocoderStatus.ZERO_RESULTS) {
							alert("No result found");
						} else {
							alert("Geocode was not successful for the following reason: " + status);
						}
					});
				}
		 	});			 

		</script>';
		$this->setInputTemplate($inputTemplate);
	}

	// public function bind($value) {
	// 	$this->setValue($value);
	// }

	// public function valid() {
		
	// }

}