<?php

namespace Lef\Forms\Fields;

class Datetime extends Field
{

	private $format = 'Y-m-d H:i:s';
	private $yearRange = Array();
	
	public function build($form, $options) {

		$this->addAttr('value', ':value');
		$this->setTemplate('<div class="form-group :error?has-error datetimepicker">
		  <label class="control-label" for=":id">
		  	:label :error
		  </label>
		  :input
		</div>');
		$this->yearRange = range(date('Y'), date('1900'));
		$this->monthRange = range(1, 31);
		$this->dayRange = range(1, 12);
	}	

	public function bind($value) {
		$datetime = new \DateTime($value);		
		$this->setValue($datetime);
	}

	public function valid() {
		// if(!filter_var($this->getValue(), FILTER_VALIDATE_EMAIL)) {
		// 	$this->setError('Veuillez renseigner un email valide');
		// }
	}

	public function renderInput($template=null) {
		if($this->getValue()) {
			$year = $this->getValue()->format('Y');
			$month = $this->getValue()->format('m');
			$day = $this->getValue()->format('d');
		}
		else {
			$year = date('Y');
			$month = date('m');
			$day = date('d');
		}
		$value = $this->getValue() ? $this->getValue()->format('Y-m-d H:i:s') : '';
		$output = '
			<input type="hidden" id="'.$this->getId().'" value="'.$value.'" name="'.$this->getName().'" />
			<select class="form-control">';
		foreach($this->yearRange as $iyear) {
			$output .= '<option '.($year == $iyear ? 'selected="selected"': '').' value="'.$iyear.'">'.$iyear.'</option>';
		}
		$output .= '		
			</select>
			<select class="form-control">';
		foreach($this->monthRange as $imonth) {
			$output .= '<option '.($month == $imonth ? 'selected="selected"': '').' value="'.$imonth.'">'.$imonth.'</option>';
		}
		$output .= '		
			</select>
			<select class="form-control">';
		foreach($this->dayRange as $iday) {
			$output .= '<option '.($day == $iday ? 'selected="selected"': '').' value="'.$iday.'">'.$iday.'</option>';
		}
		$output .= '		
			</select>
			<script type="text/javascript">
	            $("#'.$this->getId().'").each(function(self, year, month, day, change) {
	            	self = this;
	            	change = function() {
	            		var d = new Date(year.val(), month.val(), day.val()); 
	            		$(self).val(d.getFullYear()+"-"+d.getMonth()+"-"+d.getDay());
	            		console.log($(self).val());
	            	}
					year = $(this).next().change(change); 
					month = year.next().change(change); 
					day = month.next().change(change); 
	            });
	        </script>';
		return $output;
	}

}