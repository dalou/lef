<?php

namespace Lef\Forms\Fields;

class Price extends Field
{

	private $min;
	private $max;

	public function build($form, $options) {
		$this->min = !empty($options['min']) ? $options['min'] : 0;
		$this->max = !empty($options['max']) ? $options['max'] : null;
	}

	public function valid() {
		if( !filter_var($this->getValue(), FILTER_VALIDATE_INT)
			|| !filter_var($this->getValue(), FILTER_VALIDATE_FLOAT)) {
			$this->setError('Veuillez renseigner un prix valide');
		}
		else if( $this->max && $this->getValue() >= $this->max) {
			$this->setError('Veuillez renseigner un prix en dessous de '.$this->max);			
		}
		else if( $this->getValue() <= $this->min ) {
			$this->setError('Veuillez renseigner un prix supérieur à '.$this->min);			
		}
	}

}