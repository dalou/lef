<?php

namespace Lef\Forms\Fields;

class File extends Field
{
	protected $inputTemplate = '<input type="file" id=":id" value=":value" name=":name" required="required" maxlength="255" class="form-control">';

	public function build($form, $options) {

		$this->addAttr('value', ':value');
		$this->addAttr('type', 'file');
		$this->setInputTemplate('<input :attrs />');
	}

	public function bind($value) {

		$this->setValue($value);
		$entity = $this->getForm()->getEntity();
		if($entity) {
			if( $entity->setUploadedFile($value) ) {

			}
			else {
				$this->setError($entity->getFileError());
			}
		}		
	}

	public function valid() {
		
	}

}