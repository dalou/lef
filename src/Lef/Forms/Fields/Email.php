<?php

namespace Lef\Forms\Fields;

class Email extends Field
{	
	public function build($form, $options) {
		$this->addAttr('value', ':value');
		$this->addAttr('type', 'email');
	}

	public function valid() {
		if(!filter_var($this->getValue(), FILTER_VALIDATE_EMAIL)) {
			$this->setError('Veuillez renseigner un email valide');
		}
	}

}