<?php

namespace Lef\Forms\Fields;

class Textarea extends Field
{	
	public function build($form, $options) {
		$this->setInputTemplate('<textarea :attrs>:value</textarea>');
	}
}