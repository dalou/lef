<?php

namespace Lef\Forms\Fields;

class Checkbox extends Field
{	
	public function build($form, $options) {
		$this->addAttr('type', 'checkbox');
		$this->addAttr('class', '');
		$this->setTemplate('<div class="form-group :error?has-error ">	
			<label class="col-sm-2 control-label" for=":id">:label</label>
          <div class="col-sm-10">
            <div class="checkbox">		  		
		  		:input :error
            </div>
          </div>
		</div>');
		$this->setInputTemplate('<input :attrs value="1" />');
		if($this->getValue()) {
			$this->addAttr('checked', 'checked');
		}
	}

	public function bind($data) {
		$this->setValue(empty($data) ? false : true);
		if($this->getValue()) {
			$this->addAttr('checked', 'checked');
		}
	}

	public function valid() {
		// if(!filter_var($this->getValue(), FILTER_VALIDATE_BOOLEAN)) {
		// 	$this->setError('Veuillez renseigner un champ valide '.$this->getValue());
		// }
	}

}