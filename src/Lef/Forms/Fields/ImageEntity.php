<?php

namespace Lef\Forms\Fields;
use Lef\Forms\Fields\Field;

class ImageEntity extends Field
{
	public function build($form, $options) {
        $this->className = !empty($options['class']) ? $options['class'] : '';
        $this->addLink = !empty($options['addLink']) ? $options['addLink'] : '+ Image';
        $this->updateLink = !empty($options['updateLink']) ? $options['updateLink'] : 'Changer';

    }

	public function bind($value) {

		$entity = $this->getForm()->getEntity();
        if($entity) {
            $getAttrName = 'get'.ucfirst($this->getFieldName());
            $setAttrName = 'set'.ucfirst($this->getFieldName());
            $image = $entity->$getAttrName();

            if($value == 'delete') {
                if($image) {
                    $image = null;
                }
            }
            else {
                if(!$image) {
                    $image = new $this->className();
                }
                if( $image->setUploadedFile($value) ) {
                    //$image->setVinyl($vinyl);
                    $entity->$setAttrName($image);
                    //unset($images[$i]);
                }
                else {
                    $this->setError($image->getFileError());
                }
            }
        }
        $this->setValue($image);
	}

	public function renderInput($template=null) {

		$image = $this->getValue();
        $html = '
        <div class="row">
            <div id="'.$this->getId().'" class="col-md-12" style="position:relative;">
				<img style="width:150px; '.( empty($image) ? "display:none;" : "").'" class="img-thumbnail img-responsive" src="'.( empty($image) ? "" : $image->getWebPath()).'" />
				<a class="remove-image btn btn-danger btn-small" style="
                    position:absolute;
                    right:1px;
                    top: 1px;
                     '.( empty($image) ? "display:none;" : "").'">
                    <span class="glyphicon glyphicon-remove"></span>
                </a>
				<input  type="file" 
                        class="btn btn-? btn-small" 
						value="" name="'.$this->getName().'" 
						title="'.( empty($image) ? $this->addLink : $this->updateLink).'">
                <input  type="hidden" value="delete" name="" >
			</div>
        </div>

		<script type="text/javascript">
		$(document).ready(function(){		
            //$("input[type=file]").bootstrapFileInput()
			$("#'.$this->getId().'").each(function(i, self, file, img, hidden, name, remove) {

                file = $(self).find("input[type=file]");
                name = file.attr("name");
                hidden = $(self).find("input[type=hidden]");
                img = $(self).find("img");
                remove = $(self).find("a.remove-image");
                //file.bootstrapFileInput();
                file.change(function(){
                    console.log(this.files)
                    if (this.files && this.files[0]) {
                        console.log(this.files)
                        var reader = new FileReader();                      
                        reader.onload = function (e) {
                            remove.show();
                            hidden.attr("name", "");
                            file.attr("name", name).prev().text("Changer");
                            img.show().attr("src", e.target.result);
                        }                       
                        reader.readAsDataURL(this.files[0]);
                    }
                });
                remove.click(function() {
                    file.attr("name", "").prev().html("'.$this->addLink .'");
                    hidden.attr("name", name);
                    $(self).find(".file-input-name").remove();
                    img.hide();
                    remove.hide();
                });

            })
		});
		</script>';	

		return $html;
	}

}