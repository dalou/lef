<?php

namespace Lef\Forms\Fields;

class IntegerSlider extends Field
{

	private $min;
	private $max;

	public function build($form, $options) {
		$this->min = !empty($options['min']) ? $options['min'] : 0;
		$this->max = !empty($options['max']) ? $options['max'] : 100;
		$this->addAttr('value', ':value');
		$this->addAttr('data-slider-value', ':value');
		$this->addAttr('data-slider-min', $this->min);
		$this->addAttr('data-slider-max', $this->max);
		$this->addAttr('data-slider-step', 1);

		$this->setErrorTemplate('<small class="error">:error</small>');
		$this->setTemplate('<div class="form-group :has-error">
		  <label class="control-label" for=":id">
		  	:label :error
		  </label>
		  :input
		</div>

		<script>
			$("#:id").slider({
		    	formater: function(value) {
		    		return "Funded at "+value+"%";
		    	}
		    });
		</script>');

	}

	public function valid() {
		if( !filter_var($this->getValue(), FILTER_VALIDATE_INT)
			|| !filter_var($this->getValue(), FILTER_VALIDATE_FLOAT)) {
			$this->setError('Veuillez renseigner un prix valide');
		}
		else if( $this->max && $this->getValue() >= $this->max) {
			$this->setError('Veuillez renseigner un prix en dessous de '.$this->max);			
		}
		else if( $this->getValue() <= $this->min ) {
			$this->setError('Veuillez renseigner un prix supérieur à '.$this->min);			
		}
	}

}