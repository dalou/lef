<?php

namespace Lef\Forms\Fields;

class Collection extends Field
{

	private $type;
	private $class;

	public function build($form, $options) {
		$this->type = !empty($options['form']) ? $options['form'] : null;
	}

	public function bind($value) {
		print_r($value);
		$this->setValue($value);
	}

	//public function getName() { return $this->getForm()->getName().'['.$this->getFieldName().'][]'; }

	public function valid() {
		
	}

	public function renderInput() {
		$values = $this->getValue() ? $this->getValue() : Array();
			//print_r($values);
		$html = '';
		foreach($values as $value) {
			$form = new $this->formType($value, null, $this->getForm());
			$html .= $form->render();
		}

		$form = new $this->formType(null, null, $this);
		$html .= $form->render();
		$form = new $this->formType(null, null, $this);
		$html .= $form->render();
		return $html;
	}

	public function render() {

		return '<div class="form-group '.( $this->getError() ? 'has-error':'' ).'">
		  <label class="control-label" for="'.$this->getId().'">'
		  	.$this->getLabel().''.( $this->getError() ? ' : '.$this->renderError():'' ).'
		  </label>
		  '.$this->renderInput().'
		</div>';
	}

}