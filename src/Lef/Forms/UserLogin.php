<?php

namespace Lef\Forms;

use Lef\Request;
use Lef\App;
use Lef\Routes;
use Lef\Models\User as User;

class UserLogin extends Form
{

	protected function build($entity) {

		$this->addField('email', 'Lef\Forms\Fields\Email', array(
			'label' => 'Email', 
			'required' => 'Vous devez renseignez un email'
		));
		$this->addField('password', 'Lef\Forms\Fields\Password', array(
			'label' => 'Password', 
			'required' => 'Vous devez renseignez un password'
		));
		$this->addField('remember_me', 'Lef\Forms\Fields\Checkbox', array(
			'label' => 'Remember me'
		));
	}

	protected function valid() {

        $email = $this->getField('email');
		$password = $this->getField('password');
		$rememberMe = $this->getField('remember_me');

    	$rp = App::getManager('Entities\User');
        $testuser = $rp->findOneByEmail($email->getValue());
        if($testuser) {
        	if( User::encodePassword($password->getValue()) == $testuser->getPassword()) {
	            $user = $testuser;
	            $rp->logIn($user, $rememberMe->getValue());
	        }
	        else {
	            $email->setError('Bad creditencial');
	        }
        }
        else {
            $email->setError('Unknow email');
        }
	}

}