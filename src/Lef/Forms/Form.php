<?php

namespace Lef\Forms;

use Lef\App;

class Form
{
	private $entity;
	public function getEntity() { return $this->entity;	}
	public function setEntity($entity) { $this->entity = $entity;	}

	private $fields;
	public function getFields() { return $this->fields;	}
	public function getField($name) { return !empty($this->fields[$name]) ? $this->fields[$name] : null; }	
	public function addField($name, $type, $options=Array()) {		
		$this->fields[$name] = new $type($this, $name, $options);
	}

	private $errors;
	public function getErrors() { return $this->errors;	}
	public function getError($name) { return $this->errors[$name]; }
	public function addError($name, $error) { $this->errors[$name] = $error; }

	private $data;
	public function getData() { return $this->data; }

	private $context;

	private $validated;
	public function isValidated() { return $this->validated; }

	private $parentField;
	public function setParentField($parentField) { $this->parentField = $parentField;	}
	public function getParentField() { return $this->parentField;	}

	public function __construct($entity=null, $context=null, $parentField=null) {
		$this->entity = $entity;
		$this->fields = Array();
		$this->errors = Array();
		$this->data = Array();
		$this->validated = false;
		$this->parentField = $parentField;

		$this->build($entity);
	}


	public function getName() { 
		$name = '';
		if($this->getEntity()) {
			$name .= preg_replace('/\\\/i', '_',  strtolower(get_class($this->getEntity())));
		}
		if($this->getParentField()) {
			$name .= $this->getParentField()->getName();
		}
		if(empty($name)) {
			$name .= preg_replace('/\\\/i', '_',  strtolower(get_class($this)));
		}
		return $name;
	}
	

	protected function build($entity) { 

		if($entity) {
			$metadata = App::$em->getClassMetadata(get_class($entity));
			//print_r($metadata);
		}
	}
	
	public function submit($request, $context=null) {
		$this->context = $context;
		if(!empty($this->context) && empty($request[$this->context])) {
			return;
		}
		$data = $this->handleNativeRequest($request);
		//echo '<pre>'; print_r($data); echo '</pre>';
		if(!empty($data)) {
			$this->data = $data;
			$this->bind();
			$this->valid();
			if(!count($this->getErrors())) {
				$this->validated = true;
			}
		}		
	}

	protected function bind() {
		foreach($this->getFields() as $field) {
			//if( !empty($this->data[$field->getFieldName()]) ) {
			$field->bind(!empty($this->data[$field->getFieldName()]) ? $this->data[$field->getFieldName()] : null);
			$field->valid();
			//}
			//if($field->isRequired() && empty($field->getValue())) {
			$value = $field->getValue();
			if($field->isRequired() && empty($value)) {
				$field->setError($field->getRequired());
			}
		}
	}

	public function save() {
		foreach($this->getFields() as $field) {
			$field->save();	
		}
		if($this->getEntity()) {
			App::getEntityManager()->persist($this->getEntity());
			App::getEntityManager()->flush();
		}
	}

	protected function valid() { } 

	public function renderContext() {
		return !empty($this->context) ? '<input type="hidden" name="'.$this->context.'" value="1"/>': '';
	}

	public function render($name=null, $template=null) {
		if(isset($name)) {
			$field = $this->getField($name);
			if($field) {
				return $field->render($template);
			}
			else {
				return 'Unknow field name : '.$name;
			}
		}
		else {
			$html = $this->renderContext();
			foreach($this->getFields() as $field) {
				if(!$field->isRendered) {
					$html .= $field->render($template);
					$field->isRendered = true;
				}
			}
			return $html;
		}
	}












	/*
	* Fix FILES et serialize data
	*/
	private function handleNativeRequest($request) {

		$fileKeys = array(
	        'error',
	        'name',
	        'size',
	        'tmp_name',
	        'type',
	    );

		$name = $this->getName();

		$fixedFiles = array();
        foreach ($_FILES as $name => $file) {
            $fixedFiles[$name] = $this->stripEmptyFiles($this->fixPhpFilesArray($file, $fileKeys), $fileKeys);
        }
        if ('' === $name) {
            $params = $_POST;
            $files = $fixedFiles;
        } elseif (array_key_exists($name, $_POST) || array_key_exists($name, $fixedFiles)) {
            $default = array();
            $params = array_key_exists($name, $_POST) ? $_POST[$name] : $default;
            $files = array_key_exists($name, $fixedFiles) ? $fixedFiles[$name] : $default;
        } else {
            // Don't submit the form if it is not present in the request
            return null;
        }

        if (is_array($params) && is_array($files)) {
            $data = array_replace_recursive($params, $files);
        } else {
            $data = $params ?: $files;
        }
        return $data;
	}

	private function fixPhpFilesArray($data, $fileKeys)
    {
        if (!is_array($data)) {
            return $data;
        }

        $keys = array_keys($data);
        sort($keys);

        if ($fileKeys !== $keys || !isset($data['name']) || !is_array($data['name'])) {
            return $data;
        }

        $files = $data;
        foreach ($fileKeys as $k) {
            unset($files[$k]);
        }

        foreach (array_keys($data['name']) as $key) {
            $files[$key] = $this->fixPhpFilesArray(array(
                'error'    => $data['error'][$key],
                'name'     => $data['name'][$key],
                'type'     => $data['type'][$key],
                'tmp_name' => $data['tmp_name'][$key],
                'size'     => $data['size'][$key]
            ), $fileKeys);
        }

        return $files;
    }

    private function stripEmptyFiles($data, $fileKeys)
    {
        if (!is_array($data)) {
            return $data;
        }

        $keys = array_keys($data);
        sort($keys);

        if ($fileKeys === $keys) {
            if (UPLOAD_ERR_NO_FILE === $data['error']) {
                return null;
            }

            return $data;
        }

        foreach ($data as $key => $value) {
            $data[$key] = $this->stripEmptyFiles($value, $fileKeys);
        }

        return $data;
    }

}