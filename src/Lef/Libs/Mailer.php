<?php

namespace Lef\Libs;

require __DIR__.'/PHPMailer-master/PHPMailerAutoload.php';

class Mailer {

	protected static $port = 25;
	public static function setPort($port) {
		self::$port = $port;
	}

	protected static $smtp = false;
	public static function setSmtp($smtp) {
		self::$smtp = $smtp;
	}

	protected static $host = '127.0.0.1';
	public static function setHost($host) {
		self::$host = $host;
	}

	protected static $username = '';
	public static function setUsername($username) {
		self::$username = $username;
	}

	protected static $password = '';
	public static function setPassword($password) {
		self::$password = $password;
	}

	protected static $subject = '';
	public static function setSubject($subject) {
		self::$subject = $subject;
	}

	protected static $from = '';
	protected static $fromName = '';
	public static function setFrom($from, $fromName) {
		self::$from = $from;
		self::$fromName = $fromName;
	}

	protected static $to = array();
	public static function setTo($to) {
		if(is_array($to)) {
			self::$to = array_merge(self::$to, $to);
		}
		else {
			self::$to[] = $to;
		}
	}

	public static function clear() {
		
	}

	protected static $body = '';
	public static function setBody($body) {
		self::$body = $body;
	}

	protected static $altBody = '';
	public static function setAltBody($altBody) {
		self::$altBody = $altBody;
	}

	protected static $error = null;
	public static function getError() {
		return self::$error;
	}

	public static function send() 
	{
		$mail = new \PHPMailer(true);
		try {
			self::$error = null;
			//Tell PHPMailer to use SMTP
			$mail->isSMTP();
			//Enable SMTP debugging
			// 0 = off (for production use)
			// 1 = client messages
			// 2 = client and server messages
			$mail->SMTPDebug = 2;
			//Ask for HTML-friendly debug output
			$mail->Debugoutput = 'html';
			//Set the hostname of the mail server
			$mail->Host = self::$host;
			//Set the SMTP port number - likely to be 25, 465 or 587
			$mail->Port = self::$smtp;
			//Whether to use SMTP authentication
			$mail->SMTPAuth = self::$smtp;
			//Username to use for SMTP authentication
			$mail->Username = self::$username;
			//Password to use for SMTP authentication
			$mail->Password = self::$password;

			//Set who the message is to be sent from
			$mail->setFrom(self::$from, self::$fromName);
			//Set an alternative reply-to address
			// $mail->addReplyTo('autrusseau.damien@gmail.com', 'First Last');
			//Set who the message is to be sent to
			foreach(self::$to as $to) {
				$mail->addAddress($to, '-');
			}
			//Set the subject line
			$mail->Subject = self::$subject;
			//Read an HTML message body from an external file, convert referenced images to embedded,
			//convert HTML into a basic plain-text alternative body
			$mail->msgHTML(self::$body, self::$body);
			//Replace the plain text body with one created manually

			$mail->AltBody = strip_tags(self::$altBody);
			//Attach an image file
			//$mail->addAttachment('images/phpmailer_mini.gif');

			//send the message, check for errors
			if ($mail->send()) {
			   return true;
			} 
			else {
				self::$error = $mail->ErrorInfo;
			}
		} catch (\phpmailerException $e) {
		    self::$error = $e->errorMessage(); //Pretty error messages from PHPMailer
		} catch (\Exception $e) {
		    self::$error = $e->getMessage(); //Boring error messages from anything else!
		}
		return false;
	}

}