<?php

namespace Lef\Libs;

class Url {

	public static function setParam( $key, $value, $url) {
	    $info = parse_url( $url );
	    parse_str( $info['query'], $query );
	    return $info['path'] . '?' . http_build_query( $query ? array_merge( $query, array($key => $value ) ) : array( $key => $value ) );
	}

}