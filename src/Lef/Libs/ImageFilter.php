<?php

namespace Lef\Libs;

class ImageFilter {

	protected static $media_path = '/../../../www/medias/';
	protected static $media_public_path = '/medias/';
	protected static $upload_path = '../../../www/';

	public static function setPublicPath($path) {
		self::$media_public_path = $path;
	}

	public static function setUploadPath($path) {
		self::$upload_path = $path;
	}

	public static function setPath($path) {
		self::$media_path = $path;
	}

	protected static $formats = array(
		'16x16' => 		array('width' => 16, 	'height' => 16, 	'fit' => false),
		'50x40' => 		array('width' => 50, 	'height' => 40, 	'fit' => false),
		'90x60' => 		array('width' => 90, 	'height' => 60, 	'fit' => false),
		'200x200' => 	array('width' => 200, 	'height' => 200, 	'fit' => false),
		'262x255' => 	array('width' => 265, 	'height' => 255, 	'fit' => false),
		'310x300' => 	array('width' => 310, 	'height' => 300, 	'fit' => false),
		'610x600' => 	array('width' => 610, 	'height' => 600, 	'fit' => false),
		'640x480' => 	array('width' => 640, 	'height' => 480, 	'fit' => false),

		// 'SLIDER' => 		array('width' => 300, 	'height' => 300, 	'cropcenter' => true),
		// 'SLIDER_LARGE' => 	array('width' => 470, 	'height' => 470, 	'cropcenter' => true),
		// 'LARGE' => 			array('width' => 400, 	'height' => 400, 	'fit' => false),
		// 'ORIGINAL' => 		array('width' => 150, 	'height' => 150, 	'fit' => false),
	);
	public static function addFormat($name, $options=array('width' => 40, 'height' => 40, 'fit' => false)) {
		self::$formats[$name] = $options;
	}

	protected static $available_exts = array(
		'PNG' => 	array('imagecreate' => 'imagecreatefrompng', 'imagesave' => 'imagepng'),
		'JPG' => 	array('imagecreate' => 'imagecreatefromjpeg', 'imagesave' => 'imagejpeg'),
		'JPEG' => 	array('imagecreate' => 'imagecreatefromjpeg', 'imagesave' => 'imagejpeg'),
		'GIF' => 	array('imagecreate' => 'imagecreatefromgif', 'imagesave' => 'imagegif'),
	);

	private static function mkpath($path)
	{
		$path = dirname($path);
		return is_dir($path) or @mkdir($path, 0755, true);
	}

	public static function filter($url, $size, $force=false) {

		$ext = strtoupper(pathinfo($url, PATHINFO_EXTENSION));

		if(array_key_exists($size, self::$formats) && array_key_exists($ext, self::$available_exts)) {

			$dims = self::$formats[$size];



			$url_final = preg_replace('/http\:\/\//', '', $url);

			$path = join('/', array(rtrim(self::$media_path, '/'), $size, trim($url_final, '/')));

			$url = 'http://'.$_SERVER['HTTP_HOST'].'/'.$url;
			$public_path = join('/', array(rtrim(self::$media_public_path, '/'), $size, trim($url_final, '/')));

			// $upload_path = join('/', array(rtrim(self::$upload_path, '/'), trim($url_final, '/')));
			// $upload_public_path = $url;
			
			//var_dump($media_path);

			if(file_exists($path) && !$force) {
				//var_dump($media_public_path);
			}
			else {

				/* read the source image */
				$fct = self::$available_exts[$ext]['imagecreate'];
				//$source_image = $fct(rawurlencode($url)); 
				//try {
				//echo str_replace(" ", "%20", $url);
					$source_image = $fct(str_replace(" ", "%20", $url)); 
				 	if(!$source_image) {
				 		$source_image = $fct(str_replace(" ", "%20", __DIR__.'/../web/'.$url)); 
				 	}
				 	if(!$source_image) {
				 		return '';
				 	}
				// }
				// catch(Exception $e) {
				// 	return '';
				// }

				$ow = imagesx($source_image);
				$oh = imagesy($source_image);	

				if(!empty($dims['cropcenter'])) {

				   $virtual_image = imagecreatetruecolor($dims['width'], $dims['height']);
					if ($ow > $oh) {
					   $off_w = ($ow-$oh)/2;
					   $off_h = 0;
					   $ow = $oh;
					} elseif ($oh > $ow) {
					   $off_w = 0;
					   $off_h = ($oh-$ow)/2;
					   $oh = $ow;
					} else {
					   $off_w = 0;
					   $off_h = 0;
					}
					imagecopyresampled($virtual_image, $source_image, 0, 0, $off_w, $off_h, $dims['width'], $dims['height'], $ow, $oh);

					// //$desired_height = floor($height * ($dims['width'] / $width));
					// $virtual_image = imagecreatetruecolor($dims['width'], $desired_height);
					// $dx = ( $width - $dims['width'] ) / 2;
					// $dy = ( $height - $dims['height'] ) / 2; 
					// //imagecopyresized($virtual_image, $source_image, 0, 0, 0, 0, $dims['width'], $desired_height, $width, $height);
					// imagecopyresampled($virtual_image, $source_image, $dx, $dy, 0, 0, $dims['width'], $dims['height'], $width, $height);
				}	
				else if(!empty($dims['fit'])) {		
					// $oh = $dims['height'];		
					// $ow = $dims['width'];		
					$desired_height = floor($oh * ($dims['width'] / $ow));
					$virtual_image = imagecreatetruecolor($dims['width'], $dims['height']);
					//imagecopyresized($virtual_image, $source_image, 0, 0, 0, 0, $dims['width'], $desired_height, $width, $height);
					imagecopyresampled($virtual_image, $source_image, 0, 0, 0, 0, $dims['width'], $dims['height'], $ow, $oh);
				}
				else {					
					$desired_height = floor($oh * ($dims['width'] / $ow));
					$virtual_image = imagecreatetruecolor($dims['width'], $desired_height);
					//imagecopyresized($virtual_image, $source_image, 0, 0, 0, 0, $dims['width'], $desired_height, $width, $height);
					imagecopyresampled($virtual_image, $source_image, 0, 0, 0, 0, $dims['width'], $desired_height, $ow, $oh);
				}		
				
				
				// /* create the physical thumbnail image to its destination */
				self::mkpath($path);
				//closedir(dirname($path));
				$fct = self::$available_exts[$ext]['imagesave'];
				$fct($virtual_image, $path);
				imagedestroy($virtual_image);
				clearstatcache();
							
			}
			return $public_path;

			
		}
		else {
			return '';
		}
	}

}

?>