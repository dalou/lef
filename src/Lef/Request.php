<?php

namespace Lef;

class Request {

	private static $data = Array();

	public static function set($name, $value) {
		self::$data[$name] = $value;
	}

	public static function get($name) {
		return !empty(self::$data[$name]) ? self::$data[$name] : null;
	}

	public static function redirect($url) {
		try{ header('Location: '.$url); } catch(\Exception $e) {}
		echo '<script type="text/javascript">
			window.location = "'.$url.'"
		</script>';
		exit(0);
	}

	public static function isXhr() {
		return isset($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) === 'xmlhttprequest';
	} 


}