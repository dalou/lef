<?php

namespace Lef\Models;

use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Lef\Models\Dated;

/**
 * @ORM\MappedSuperclass
 * @ORM\HasLifecycleCallbacks
 */
class GeoPosition extends Dated
{ 
    public function __construct()
    {
    }

    /**
     * @ORM\Column(type="float", nullable=true)
     */
    private $latitude;
    public function setLatitude($latitude) { $this->latitude = $latitude; return $this; }
    public function getLatitude() { return $this->latitude; }

    /**
     * @ORM\Column(type="float", nullable=true)
     */
    private $longitude;
    public function setLongitude($longitude) { $this->longitude = $longitude; return $this; }
    public function getLongitude() { return $this->longitude; }

    /**
     * @ORM\Column(type="string", nullable=true)
     */
    private $originalAddress;
    public function setOriginalAddress($originalAddress) { $this->originalAddress = $originalAddress; return $this; }
    public function getOriginalAddress() { return $this->originalAddress; }

    /**
     * @ORM\Column(type="string", nullable=true)
     */
    private $placeName;
    public function setPlaceName($placeName) { $this->placeName = $placeName; return $this; }
    public function setFormattedAddress($placeName) { $this->placeName = $placeName; return $this; }
    public function getPlaceName() { return $this->placeName; }
    public function getFormattedAddress() { return $this->placeName; }

    /**
     * @ORM\Column(type="string", nullable=true)
     */
    private $placeType;
    public function setPlaceType($placeType) { $this->placeType = $placeType; return $this; }
    public function getPlaceType() { return $this->placeType; }

    /**
     * @ORM\Column(type="string", nullable=true)
     */
    private $streetNumber;
    public function setStreetNumber($streetNumber) { $this->streetNumber = $streetNumber; return $this; }
    public function getStreetNumber() { return $this->streetNumber; }

    /**
     * @ORM\Column(type="string", nullable=true)
     */
    private $route;
    public function setRoute($route) { $this->route = $route; return $this; }
    public function getRoute() { return $this->route; }

    /**
     * @ORM\Column(type="string", nullable=true)
     */
    private $locality;
    public function setLocality($locality) { $this->locality = $locality; return $this; }
    public function getLocality() { return $this->locality; }

    /**
     * @ORM\Column(type="string", nullable=true)
     */
    private $administrativeAreaLevel2;
    public function setAdministrativeAreaLevel2($administrativeAreaLevel2) { $this->administrativeAreaLevel2 = $administrativeAreaLevel2; return $this; }
    public function setDepartment($administrativeAreaLevel2) { $this->administrativeAreaLevel2 = $administrativeAreaLevel2; return $this; }
    public function getAdministrativeAreaLevel2() { return $this->administrativeAreaLevel2; }
    public function getDepartment() { return $this->administrativeAreaLevel2; }

    /**
     * @ORM\Column(type="string", nullable=true)
     */
    private $administrativeAreaLevel1;
    public function setAdministrativeAreaLevel1($administrativeAreaLevel1) { $this->administrativeAreaLevel1 = $administrativeAreaLevel1; return $this; }
    public function setRegion($administrativeAreaLevel1) { $this->administrativeAreaLevel1 = $administrativeAreaLevel1; return $this; }
    public function getAdministrativeAreaLevel1() { return $this->administrativeAreaLevel1; }
    public function getRegion() { return $this->administrativeAreaLevel1; }

    /**
     * @ORM\Column(type="string", nullable=true)
     */
    private $country;
    public function setCountry($country) { $this->country = $country; return $this; }
    public function getCountry() { return $this->country; }

    /**
     * @ORM\Column(type="string", nullable=true)
     */
    private $postalCode;
    public function setPostalCode($postalCode) { $this->postalCode = $postalCode; return $this; }
    public function setZipCode($postalCode) { $this->postalCode = $postalCode; return $this; }
    public function getPostalCode() { return $this->postalCode; }
    public function getZipCode() { return $this->postalCode; }


    public function getDistanceFrom($lat2, $lon2, $unit="K", $round=true) { 

        $lat1 = $this->latitude;
        $lon1 = $this->longitude;
        $theta = $lon1 - $lon2;
        $dist = sin(deg2rad($lat1)) * sin(deg2rad($lat2)) +  cos(deg2rad($lat1)) * cos(deg2rad($lat2)) * cos(deg2rad($theta));
        $dist = acos($dist);
        $dist = rad2deg($dist);
        $miles = $dist * 60 * 1.1515;
        $unit = strtoupper($unit);

        if ($unit == "K") { // Kilometers        
            $result = ($miles * 1.609344);        
        } else if ($unit == "N") { // Nautical Miles        
            $result = ($miles * 0.8684);
        } else {        
            $result = $miles; // Miles            
        } 
        if($round) { $result = round($result, 1); } 
        return $result;
    }
}
