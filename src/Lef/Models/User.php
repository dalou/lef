<?php

namespace Lef\Models;

use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;

/**
 * @ORM\MappedSuperclass
 * @ORM\HasLifecycleCallbacks
 **/
class User extends Dated
{
    public function __construct()
    {
    }

    /** 
     * @ORM\Column(type="string", unique=true, nullable=true) 
    **/
    protected $facebookId;
    public function getFacebookId() { return $this->facebookId; }
    public function setFacebookId($facebookId) { $this->facebookId = $facebookId; }
    public function isFacebook() { return !empty($this->facebookId); }

    /** 
     * @ORM\Column(type="string", nullable=true) 
    **/
    protected $password;
    public function getPassword() { return $this->password; }
    public function setPassword($password) { $this->password = $password; }

    /** 
     * @ORM\Column(type="string", nullable=true) 
    **/
    protected $loginSession;
    public function setLoginSession($loginSession) { $this->loginSession = $loginSession; }

    /** 
    **/
    protected $plainPassword;
    public function getPlainPassword() { return $this->plainPassword; }
    public function setPlainPassword($plainPassword) { 
        $plainPassword = trim($plainPassword);
        if(!empty($plainPassword)) { 
            $this->setPassword(self::encodePassword($plainPassword));  
        }
    }

    public static function encodePassword($password) {
        return hash('sha512', $password);
    }

     /** 
     * @ORM\Column(type="string", nullable=true) 
    **/
    protected $salt;
    public function getSalt() { return $this->salt; }
    public function setSalt($salt) { $this->salt = $salt; }

    /** 
     * @ORM\Column(type="string", unique=true) 
    **/
    protected $email;
    public function getEmail() { return $this->email; }
    public function setEmail($email) { $this->email = $email; }

    /** 
     * @ORM\Column(type="string", unique=true, nullable=true) 
    **/
    protected $username;
    public function setUsername($username) { $this->username = $username; } 
    public function getUsername() { return $this->username; }

    /** 
     * @ORM\Column(type="string", nullable=true) 
    **/
    protected $firstname;
    public function setFirstname($firstname) { $this->firstname = $firstname; }    
    public function getFirstname() { return $this->firstname; }

    /** 
     * @ORM\Column(type="string", nullable=true) 
    **/
    protected $lastname; 
    public function getLastname() { return $this->lastname; }
    public function setLastname($lastname) { $this->lastname = $lastname; }

    /** 
     * @ORM\Column(type="string") 
    **/
    protected $level;
    public function getLevel() { return $this->level; }
    public function setLevel($level) { $this->level = $level; }

    public function isSuperAdmin() {
        return $this->getLevel() == 'SUPERADMIN';
    }

    public function isAdmin() {
        return $this->getLevel() == 'ADMIN' || $this->isSuperAdmin();
    }

    public function isWritter() {
        return $this->getLevel() == 'WRITTER' || $this->isSuperAdmin();
    }

    public function isMember() {
        return $this->getLevel() == 'MEMBER' || $this->isWritter();
    }

    public function isVisitor() {
        return !$this->isLogged();
    }

    public function isLogged() {
        return !empty($_SESSION['user']['logged']) && $_SESSION['user']['logged'] == $this->getId();
    }

    public function isActive() {
        return $this->get('status') == "IS_ACTIVE";
    }        



}