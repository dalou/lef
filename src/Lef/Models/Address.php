<?php

namespace Entities;

use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Lef\Models\Dated;
use Lef\App;

/**
 * @ORM\Entity(repositoryClass="Managers\Address")
 * @ORM\HasLifecycleCallbacks
 * @ORM\Table(name="address")
 */
class Address extends Dated { 
    
    /**
    * @ORM\Column(type="string", length=255)
    */
    private $country;
    public function setCountry($country) { $this->country = $country; return $this; }
    public function getCountry() { return $this->country; }

    /**
    * @ORM\Column(type="string", length=255)
    */
    private $province;
    public function setProvince($province) { $this->province = $province; return $this; }
    public function getProvince() { return $this->province; }

    /**
    * @ORM\Column(type="string", length=255)
    */
    private $firstName;
    public function setFirstName($firstName) { $this->firstName = $firstName; return $this; }
    public function getFirstName() { return $this->firstName; }

    /**
    * @ORM\Column(type="string", length=255)
    */
    private $lastName;
    public function setLastName($lastName) { $this->lastName = $lastName; return $this; }
    public function getLastName() { return $this->lastName; }

    /**
    * @ORM\Column(type="string", length=255)
    */
    private $street;
    public function setStreet($street) { $this->street = $street; return $this; }
    public function getStreet() { return $this->street; }

    /**
    * @ORM\Column(type="string", length=255)
    */
    private $company;
    public function setCompany($company) { $this->company = $company; return $this; }
    public function getCompany() { return $this->company; }

    /**
    * @ORM\Column(type="string", length=255)
    */
    private $city;
    public function setCity($city) { $this->city = $city; return $this; }
    public function getCity() { return $this->city; }

    /**
    * @ORM\Column(type="string", length=255)
    */
    private $postcode;
    public function setPostcode($postcode) { $this->postcode = $postcode; return $this; }
    public function getPostcode() { return $this->postcode; }

    /**
     * ORM\ManyToOne(targetEntity="Entities\User")
    */
    // private $user;
    // public function setUser(\Entities\User $user = null) { $this->user = $user; return $this; }
    // public function getUser() { return $this->user; }

}
